### Netzon Docker QA

A docker definition based on `netzon/netzon-docker` with a headless Google Chrome from Google typically used for end-to-end testing of Angular & Ionic Applications.

### Changes:

1. Installed NodeJS
1. Installed Python 3
1. Dot Net Core 2.1